adb reboot fastboot

fastboot flash bootloader out/target/product/universal9820/bootloader.img
fastboot flash dtb out/target/product/universal9820/dtb.img
fastboot flash dtbo out/target/product/universal9820/dtbo.img
fastboot flash boot out/target/product/universal9820/boot.img

fastboot flash vendor out/target/product/universal9820/vendor.img -S 300M
fastboot flash system out/target/product/universal9820/system.img -S 512M
fastboot flash vbmeta out/target/product/universal9820/vbmeta.img
fastboot reboot -w
