/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Fastboot.h"

#include <android-base/logging.h>
#include "pit_partition.h"

namespace android {
namespace hardware {
namespace fastboot {
namespace V1_0 {
namespace implementation {


android::hardware::fastboot::V1_0::FileSystemType get_fs_type(struct pit_entry *pit_entry_table)
{
	if (pit_entry_table->filesys == FS_TYPE_SPARSE_EXT4) {
		return FileSystemType::EXT4;
	} else if (pit_entry_table->filesys == FS_TYPE_SPARSE_F2FS) {
		return FileSystemType::F2FS;
	} else {
		return FileSystemType::RAW;
	}
}

// Methods from ::android::hardware::fastboot::V1_0::IFastboot follow.
Return<void> Fastboot::getPartitionType(const hidl_string& partitionName,
				getPartitionType_cb _hidl_cb) {

	struct pit_entry *pit_table;
	int offset = PIT_BLK_START*PIT_SECTOR_SIZE + sizeof(struct pit_header);
	int rc = -1;

	const char* data = partitionName.c_str();

//	pit_table = (struct pit_entry *)aligned_alloc(sizeof(struct pit_entry) , sizeof(struct pit_entry));
	pit_table = (struct pit_entry *)malloc(sizeof(struct pit_entry));
	memset(pit_table, 0, sizeof(struct pit_entry));

	/* replace /dev/block/XXX with the source block device */
	int fd = open("/dev/block/sda", O_RDONLY);

	if (fd < 0) {
		LOG(ERROR) << "file open error" ;
	}

	lseek(fd, offset, SEEK_SET);

	for (int i = 0; i < PIT_MAX_PART_NUM; i++) {
		rc = read(fd, pit_table, sizeof(struct pit_entry));
		if (rc < 0)
			LOG(ERROR) << "READ Error in pit_entry #" << i;

		if (!strcmp(data, (const char*)pit_table->name)) {
			LOG(INFO) << "Partition Name : " << data << "found in part entry";
			break;
		}
	}
	_hidl_cb(get_fs_type(pit_table), {Status::SUCCESS, ""});
	free(pit_table);
	return Void();
}

Return<void> Fastboot::doOemCommand(const hidl_string& /* oemCmd */, doOemCommand_cb _hidl_cb) {
    _hidl_cb({Status::FAILURE_UNKNOWN, "Command not supported in default implementation"});
    return Void();
}

Return<void> Fastboot::getVariant(getVariant_cb _hidl_cb) {
    _hidl_cb("NA", {Status::SUCCESS, ""});
    return Void();
}

Return<void> Fastboot::getOffModeChargeState(getOffModeChargeState_cb _hidl_cb) {
    _hidl_cb(false, {Status::SUCCESS, ""});
    return Void();
}

Return<void> Fastboot::getBatteryVoltageFlashingThreshold(
        getBatteryVoltageFlashingThreshold_cb _hidl_cb) {
    _hidl_cb(0, {Status::SUCCESS, ""});
    return Void();
}

extern "C" IFastboot* HIDL_FETCH_IFastboot(const char* /* name */) {
    return new Fastboot();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace fastboot
}  // namespace hardware
}  // namespace android
