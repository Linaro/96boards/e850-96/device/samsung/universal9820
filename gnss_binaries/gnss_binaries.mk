#only GPS libraries and binaries to the target directory
GPS_ROOT := device/samsung/universal9820/gnss_binaries/release

# PRODUCT_COPY_FILES += \
    $(GPS_ROOT)/vendor.samsung.hardware.gnss@1.0-service:vendor/bin/hw/vendor.samsung.hardware.gnss@1.0-service \
    $(GPS_ROOT)/vendor.samsung.hardware.gnss@1.0-impl.so:vendor/lib64/hw/vendor.samsung.hardware.gnss@1.0-impl.so \
    $(GPS_ROOT)/android.hardware.gnss@1.1-impl.so:vendor/lib64/hw/android.hardware.gnss@1.1-impl.so \
    $(GPS_ROOT)/android.hardware.gnss@1.0-impl.so:vendor/lib64/hw/android.hardware.gnss@1.0-impl.so \
    $(GPS_ROOT)/vendor.samsung.hardware.gnss@1.0.so:vendor/lib64/vendor.samsung.hardware.gnss@1.0.so

