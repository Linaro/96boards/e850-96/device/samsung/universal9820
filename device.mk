#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


include $(LOCAL_PATH)/BoardConfig.mk

ifneq ($(filter P%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
PRODUCT_SHIPPING_API_LEVEL := 28
BOARD_SYSTEMSDK_VERSIONS := 28
BOARD_BOOT_HEADER_VERSION := 1
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
PREBUILD_PATH_BASE := vendor/samsung_slsi/exynos9820/prebuilts/universal9820
CONF_PATH_BASE := device/samsung/universal9820/conf_p
FSTAB_PATH_BASE := device/samsung/universal9820/conf_p
BOARD_USES_KEYMASTER_VER1 := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
else
ifneq ($(filter Q%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
PRODUCT_SHIPPING_API_LEVEL := 29
BOARD_SYSTEMSDK_VERSIONS := 29
BOARD_BOOT_HEADER_VERSION := 1
PRODUCT_USE_DYNAMIC_PARTITIONS := true
PREBUILD_PATH_BASE := vendor/samsung_slsi/exynos9820/prebuilts/universal9820_q
CONF_PATH_BASE := device/samsung/universal9820/conf_q
FSTAB_PATH_BASE := device/samsung/universal9820/conf_q
BOARD_USES_KEYMASTER_VER1 := true
BOARD_USES_EXYNOS_AFBC_FEATURE := true
else
ifneq ($(filter R%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
PRODUCT_SHIPPING_API_LEVEL := 30
BOARD_SYSTEMSDK_VERSIONS := 30
BOARD_BOOT_HEADER_VERSION := 2
PRODUCT_USE_DYNAMIC_PARTITIONS := true
BOARD_USES_METADATA_PARTITION := true
PREBUILD_PATH_BASE := vendor/samsung_slsi/exynos9820/prebuilts/universal9820_r
CONF_PATH_BASE := device/samsung/universal9820/conf_r
FSTAB_PATH_BASE := device/samsung/universal9820/conf_r
BOARD_USES_KEYMASTER_VER1 := true
USE_SWIFTSHADER := false
BOARD_USES_EXYNOS_AFBC_FEATURE := false
endif
endif
endif

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts
ifeq ($(wildcard $(BOARD_PREBUILTS)),)
INSTALLED_KERNEL_TARGET := $(PREBUILD_PATH_BASE)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(PREBUILD_PATH_BASE)/dtbo.img
BOARD_PREBUILT_DTB := $(PREBUILD_PATH_BASE)/universal9820.dtb
BOARD_PREBUILT_BOOTLOADER_IMG := $(PREBUILD_PATH_BASE)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
ifeq ($(BOARD_KERNEL_MOUDLES),true)
BOARD_PREBUILT_KERNEL_MODULES := $(PREBUILD_PATH_BASE)/modules
BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS := $(PREBUILD_PATH_BASE)/modules_symbols
endif
PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel \
		device/samsung/universal9820/flashall.sh:$(PRODUCT_OUT)/flashall.sh \
		device/samsung/universal9820/flashall.bat:$(PRODUCT_OUT)/flashall.bat
else
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
ifeq ($(BOARD_KERNEL_MOUDLES),true)
BOARD_PREBUILT_KERNEL_MODULES := $(BOARD_PREBUILTS)/modules
BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS := $(BOARD_PREBUILTS)/modules_symbols
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB) $(BOARD_PREBUILT_KERNEL_MODULES) $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
else
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
endif
endif

ifeq ($(BOARD_KERNEL_MOUDLES),true)
PRODUCT_COPY_FILES += $(foreach modules, $(wildcard $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS)/*), $(modules):$(PRODUCT_OUT)/modules_symbols/$(notdir $(modules)))
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(foreach module,$(shell cat $(BOARD_PREBUILT_KERNEL_MODULES)/modules.order),$(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(module)))
ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_KERNEL_MODULES += $(foreach module,$(shell cat $(BOARD_PREBUILT_KERNEL_MODULES)/modules.order),$(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(module)))
endif
endif

ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
PRODUCT_PACKAGES += \
	android.hardware.boot@1.1-impl \
	android.hardware.boot@1.1-service \
	bootctrl.exynos9820 \
	update_engine \
	update_verifier
endif


# recovery mode
ifneq ($(call math_gt_or_eq,$(BOARD_BOOT_HEADER_VERSION),3),)
BOARD_INCLUDE_RECOVERY_DTBO := true
ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_MKBOOTIMG_ARGS := --header_version 2
endif
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0 \
  --pagesize $(BOARD_FLASH_BLOCK_SIZE)
else
ifeq ($(BOARD_BOOT_HEADER_VERSION),2)
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0
else
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION)
endif
endif

PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel

TARGET_RECOVERY_FSTAB := $(FSTAB_PATH_BASE)/fstab.exynos9820


# Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x02800000
else
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x02900000
endif
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000

BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 471859200

BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2662400000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
BOARD_MOUNT_SDCARD_RW := true

ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x03000000
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
endif

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Dynamic Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_SUPER_PARTITION_SIZE := 3145728000
else
BOARD_SUPER_PARTITION_SIZE := 6287261696
endif

# Configuration for dynamic partitions
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 3141533696
ifeq ($(WITH_ESSI),true)
BOARD_GROUP_BASIC_PARTITION_LIST := vendor
else
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif
endif


# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    audio.offload.min.duration.secs=30

# Device Manifest, Device Compatibility Matrix for Treble
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
DEVICE_MANIFEST_FILE := \
	device/samsung/universal9820/manifest_abupdate.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal9820/framework_compatibility_matrix_abupdate.xml
else
ifneq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
DEVICE_MANIFEST_FILE := \
	device/samsung/universal9820/manifest_api30.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal9820/framework_compatibility_matrix_api30.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/universal9820/manifest.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal9820/framework_compatibility_matrix.xml
endif
endif

DEVICE_MATRIX_FILE := \
	device/samsung/universal9820/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9820/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9820/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal9820/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/universal9820/overlay

# Init files
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.exynos9820.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.exynos9820.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc \
	device/samsung/universal9820/conf/init.recovery.exynos9820.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.$(TARGET_SOC).rc \
	device/samsung/universal9820/conf/init.recovery.exynos9820.rc:root/init.recovery.$(TARGET_SOC).rc

ifeq ($(TARGET_SOC), exynos9825)
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/conf_r/ueventd.exynos9825.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc

TARGET_VENDOR_PROP := \
	device/samsung/universal9820/vendor.universal9825.prop
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/conf/ueventd.exynos9820.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc

TARGET_VENDOR_PROP := \
	device/samsung/universal9820/vendor.universal9820.prop
endif

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC)
endif
ifeq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.exynos9820:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/first_stage_ramdisk/fstab.$(TARGET_SOC)
endif
# HACK
ifeq ($(TARGET_SOC), exynos9825)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos9825
endif
else
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos9820.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
endif
endif

# Support devtools
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	Development
endif

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
ifeq ($(filter exynos9825, $(TARGET_SOC)), )
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/exynos9820/secapp/09090000070100010000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/09090000070100010000000000000000.tlbin
endif

# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.1\
       android.hardware.fastboot@1.1-impl-mock.exynos9820
endif

ifneq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
       android.hardware.power-service.exynos.982x
else
# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)
endif

ifneq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl\
	android.hardware.thermal@2.0-service.exynos
else
PRODUCT_PACKAGES += \
	android.hardware.thermal@1.0-impl\
	android.hardware.thermal@1.0-service\
	thermal.$(TARGET_SOC)
endif

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service

# configStore HAL
PRODUCT_PACKAGES += \
    android.hardware.configstore@1.0-service \
    android.hardware.configstore@1.0-impl

#
# Audio HALs
#

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1

ifneq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    android.hardware.audio@2.0-service \
    android.hardware.audio@6.0-impl \
    android.hardware.audio.effect@6.0-impl \
    android.hardware.soundtrigger@2.0-impl
else
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
    android.hardware.audio@2.0-service \
    android.hardware.audio@5.0-impl \
    android.hardware.audio.effect@5.0-impl \
    android.hardware.soundtrigger@2.0-impl
endif

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	device/samsung/universal9820/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	frameworks/av/services/audiopolicy/config/hearing_aid_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/hearing_aid_audio_policy_configuration.xml

# Mixer Path Configuration for AudioHAL
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal9820/audio/config/mixer_usb_white.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_white.xml \
	device/samsung/universal9820/audio/config/mixer_usb_gray.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_usb_gray.xml \
	device/samsung/universal9820/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# AudioEffectHAL Configuration
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# BT A2DP HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.ExynosA2DPOffload@1.0-service \
	vendor.samsung_slsi.hardware.ExynosA2DPOffload@1.0-impl

# Calliope & VTS firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/vts.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin \
	device/samsung/universal9820/firmware/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal9820/firmware/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal9820/firmware/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal9820/firmware/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal9820/firmware/calliope2.dt:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope2.dt \
	device/samsung/universal9820/firmware/usbout.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/usbout.bin \
	device/samsung/universal9820/firmware/usbin.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/usbin.bin

# AudioEffectHAL library
ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
PRODUCT_PACKAGES += \
	libexynospostprocbundle
endif
endif

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL), true)
PRODUCT_PACKAGES += \
	sound_trigger.primary.$(TARGET_SOC)
endif

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox

# TinyTools for Audio
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    tinyhostless
endif


# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# for now include gralloc here. should come from hardware/samsung_slsi/exynos.target_soc
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion_exynos \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

ifneq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    vndservicemanager
endif

ifeq ($(filter full_universal9820_r full_universal9825_r full_universal9825_r_mali, $(TARGET_PRODUCT)),)
# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
	android.hardware.drm@1.0-service
endif
# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlsecdrm \
	liboemcrypto_modular

# tlwvdrm
ifeq ($(filter exynos9825, $(TARGET_SOC)), )
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/00060308060501020000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tlbin
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/exynos9820/secapp/00060308060501020000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/00060308060501020000000000000000.tlbin
endif
# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	tee_whitelist

# tee_whitelist
#PRODUCT_COPY_FILES += \
#	vendor/samsung_slsi/$(TARGET_SOC)/secapp/public.libraries.txt:$(TARGET_COPY_OUT_VENDOR)/etc/public.libraries.txt

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos9820/mobicore
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos9820/secapp

# Camera HAL
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
ifneq ($(filter full_universal9820_evt0 full_universal9820_evt0_s5100,$(TARGET_PRODUCT)),)
 PRODUCT_COPY_FILES += \
    device/samsung/universal9820/firmware/camera/evt0/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_lib.bin \
    device/samsung/universal9820/firmware/camera/evt0/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_rta.bin \
    device/samsung/universal9820/firmware/camera/evt0/setfile_2l3.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2l3.bin \
    device/samsung/universal9820/firmware/camera/evt0/setfile_3h1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3h1.bin
else
 PRODUCT_COPY_FILES += \
    device/samsung/universal9820/firmware/camera/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_lib.bin \
    device/samsung/universal9820/firmware/camera/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_rta.bin \
    device/samsung/universal9820/firmware/camera/setfile_2l4.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2l4.bin \
    device/samsung/universal9820/firmware/camera/setfile_3j1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3j1.bin
endif

#    device/samsung/universal9820/firmware/camera/setfile_3m3.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3m3.bin \

# Copy Camera HFD Setfiles
PRODUCT_COPY_FILES += \
    device/samsung/universal9820/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/universal9820/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/universal9820/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/universal9820/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/universal9820/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2019-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1

# Epic daemon
PRODUCT_SOONG_NAMESPACES += device/samsung/universal9820/power

PRODUCT_COPY_FILES += \
        device/samsung/universal9820/power/epic.json:$(TARGET_COPY_OUT_VENDOR)/etc/epic.json

PRODUCT_PACKAGES += epic libepic_helper

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
        vendor.samsung_slsi.hardware.epic@1.0-impl \
        vendor.samsung_slsi.hardware.epic@1.0-service

# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME)

PRODUCT_PROPERTY_OVERRIDES += \
	debug.sf.disable_backpressure=1

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

# Set default USB interface
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=mtp,adb

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

####################################
## VIDEO
####################################
# MFC firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/mfc_fw_v13.0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# default service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/universal9820/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal9820/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
endif

# 2. OpenMAX IL
# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/universal9820/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
    device/samsung/universal9820/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy
####################################

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Telephony
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))

PRODUCT_COPY_FILES += device/samsung/universal9820/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# EPX firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/epx.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/epx.bin

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

#GPS
# PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.1-impl \
	vendor.samsung.hardware.gnss@1.0-impl \
	vendor.samsung.hardware.gnss@1.0-service

# Exynos OpenVX framework
PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-impl \
	android.hardware.gnss@1.0-service \
	gps.$(TARGET_SOC)

# Copy SCore FW
# SCore develop1 firmware
ifneq ($(filter full_universal9820_evt0 full_universal9820_evt0_s5100,$(TARGET_PRODUCT)),)
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/score/evt0/develop1/score_ts_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_dmb.bin \
	device/samsung/universal9820/firmware/score/evt0/develop1/score_ts_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_pmw.bin \
	device/samsung/universal9820/firmware/score/evt0/develop1/score_br_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_dmb.bin \
	device/samsung/universal9820/firmware/score/evt0/develop1/score_br_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_pmw.bin
else
PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/score/evt1/develop1/score_ts_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_dmb.bin \
	device/samsung/universal9820/firmware/score/evt1/develop1/score_ts_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_pmw.bin \
	device/samsung/universal9820/firmware/score/evt1/develop1/score_br_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_dmb.bin \
	device/samsung/universal9820/firmware/score/evt1/develop1/score_br_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_pmw.bin
endif
# SCore camera 1 firmware
 PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera1/score_dummy
# SCore camera2 firmware
 PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera2/score_dummy
# SCore camera3 firmware
 PRODUCT_COPY_FILES += \
	device/samsung/universal9820/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera3/score_dummy

ifeq ($(BOARD_USES_OPENVX),true)
# IVA firmware
PRODUCT_COPY_FILES += \
        device/samsung/universal9820/firmware/iva30_rt-makalu.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/iva30_rt-makalu.bin
endif

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# gatekeeper TA
ifeq ($(filter exynos9825, $(TARGET_SOC)), )
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/08130000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/08130000000000000000000000000000.tlbin
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/exynos9820/secapp/08130000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/08130000000000000000000000000000.tlbin
endif

#CryptoManager - tlcmdrv / tlcmtest / cm_test

ifeq ($(filter exynos9825, $(TARGET_SOC)), )
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/04010000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/04010000000000000000000000000000.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/cm_test:$(TARGET_COPY_OUT_VENDOR)/app/cm_test
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/exynos9820/secapp/FFFFFFFFD00000000000000000000016.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/FFFFFFFFD00000000000000000000016.tlbin \
	vendor/samsung_slsi/exynos9820/secapp/04010000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/04010000000000000000000000000000.tlbin \
	vendor/samsung_slsi/exynos9820/secapp/cm_test:$(TARGET_COPY_OUT_VENDOR)/app/cm_test
endif

# Eden
PRODUCT_PACKAGES += \
    android.hardware.neuralnetworks@1.3-service.eden-drv \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
    vendor.samsung_slsi.hardware.eden_runtime@1.0-service

PRODUCT_PROPERTY_OVERRIDES += \
    log.tag.EDEN=INFO \
    ro.vendor.eden.devices=CPU1_GPU1_NPU1 \
    ro.vendor.eden.npu.version.path=/sys/devices/platform/npu@17800000/version

PRODUCT_PACKAGES += \
    android.hardware.graphics.composer@2.2-impl \
    android.hardware.graphics.composer@2.2-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

ifeq ($(TARGET_SOC), exynos9825)
PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13d60000.ufs/by-name/frp
else
PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13d60000.ufs/by-name/persist
endif

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@3.0-impl \
	android.hardware.keymaster@3.0-service

ifeq ($(BOARD_USES_KEYMASTER_VER1),true)
PRODUCT_PACKAGES += \
	keystore.$(TARGET_SOC)
#tlkeymasterM
ifeq ($(filter exynos9825, $(TARGET_SOC)), )
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC)/secapp/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin
else
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/exynos9820/secapp/0706000000000000000000000000004d.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/0706000000000000000000000000004d.tlbin
endif
endif

# MALI or SWIFTSHADER
ifeq ($(USE_SWIFTSHADER),true)
PRODUCT_PACKAGES += \
	libEGL_swiftshader \
	libGLESv1_CM_swiftshader \
	libGLESv2_swiftshader

PRODUCT_PROPERTY_OVERRIDES += \
       ro.hardware.egl = swiftshader
else
PRODUCT_PACKAGES += \
	libGLES_mali \
	libGLES_mali32 \
	libmalicore \
	libmalicore32 \
	libRSDriverArm \
	libRSDriverArm32
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.egl = mali
endif

#vendor directory packages
PRODUCT_PACKAGES += \
	libbccArm_bifrost \
	libLLVM_android_mali \
	libOpenCL \
	libOpenCL32 \
	libclcore \
	libclcore32 \
	libclcore_neon \
	libbcc_mali \
	bcc_mali \
	whitelist \
	libstagefright_hdcp \
	libskia_opt

PRODUCT_PACKAGES += \
	mfc_fw.bin \
	calliope_sram.bin \
	calliope_dram.bin \
	vts.bin \
	dsm.bin \
	APBargeIn_AUDIO_SLSI.bin \
	AP_AUDIO_SLSI.bin \
	APBiBF_AUDIO_SLSI.bin \
	APDV_AUDIO_SLSI.bin \
	NPU.bin

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos9820/exynos9820.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/hal3/camera.mk)
# $(call inherit-product, device/samsung/universal9820/gnss_binaries/gnss_binaries.mk)

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)
