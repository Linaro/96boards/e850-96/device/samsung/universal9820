adb reboot bootloader

fastboot flash bootloader out/target/product/universal9825/bootloader.img
fastboot flash dtb out/target/product/universal9825/dtb.img
fastboot flash dtbo out/target/product/universal9825/dtbo.img
fastboot flash boot out/target/product/universal9825/boot.img
fastboot flash vbmeta out/target/product/universal9825/vbmeta.img
fastboot flash recovery out/target/product/universal9825/recovery.img

fastboot reboot fastboot

fastboot flash vendor out/target/product/universal9825/vendor.img
fastboot flash system out/target/product/universal9825/system.img
fastboot reboot -w
